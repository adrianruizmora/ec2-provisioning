import subprocess


def replace_variable(src_path, key, value):
    with open(src_path, "r") as file:
        filedata = file.read()
    filedata = filedata.replace(key, value)
    with open(src_path, "w") as file:
        file.write(filedata)


app_name = input("What is your app name (default: react-app) ? ") or "react-app"
repo_url = (
    input(
        "Repo URL of the application you want to build (default: git@gitlab.com:adrianruizmora/react-app.git) : "
    )
    or "git@gitlab.com:adrianruizmora/react-app.git"
)
ssh_key_name = input("SSH key name (default: id_rsa) : ") or "id_rsa"
aws_access_key = input("AWS_ACCES_KEY: ")
aws_secret_key = input("AWS_SECRET_KEY: ")

username = subprocess.check_output("whoami", shell=True).decode("utf-8").strip()
username = input(f"Username (default: {username}) : ") or username

replace_variable("nginx/nginx.conf", "$APP_NAME", app_name)

replace_variable("playbooks/aws_app_deployment.yml", "$REPO_URL", repo_url)
replace_variable("playbooks/aws_app_deployment.yml", "$APP_NAME", app_name)
replace_variable("playbooks/aws_app_deployment.yml", "$SSH_KEY_NAME", ssh_key_name)

replace_variable("playbooks/aws_ec2_provisioning.yml", "$SSH_KEY_NAME", ssh_key_name)
replace_variable("playbooks/aws_ec2_provisioning.yml", "$APP_NAME", app_name)
replace_variable(
    "playbooks/aws_ec2_provisioning.yml", "$AWS_ACCESS_KEY", aws_access_key
)
replace_variable(
    "playbooks/aws_ec2_provisioning.yml", "$AWS_SECRET_KEY", aws_secret_key
)
replace_variable("playbooks/aws_ec2_provisioning.yml", "$USERNAME", username)

replace_variable("ansible.cfg", "$USERNAME", username)
replace_variable("ansible.cfg", "$SSH_KEY_NAME", ssh_key_name)

replace_variable("inventory.ini", "$APP_NAME", app_name)
