<div id="top"></div>


<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h3 align="center">EC2 PROVISIONING WITH ANSIBLE</h3>

  <p align="center">
    <br />
    <br />
    <br />

  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
  </ol>
</details>


### Built With

* [Ansible](https://nextjs.org/)
* [React.js](https://reactjs.org/)
* [Nginx](https://www.python.org/)


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started


### Prerequisites
* AWS account with rights for manipulating EC2 services
* python
  ```sh
  sudo apt install python
  ```
* python-pip
  ```sh
  sudo apt install python-pip
  ```
* boto, boto3 and ansible
  ```sh
  pip install -r requirements.txt
  ```

### Installation

1. Install AWS ansible module
   ```sh
   ansible-galaxy collection install community.aws
   ```
2. Clone the repo or fork the project and add your ssh public key
  ```sh
   git clone https://@gitlab.com:adrianruizmora/ec2-provisioning.git
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

1. Run start.py file on the root directory to set ansible variables
   ```sh
   python3 start.py
   ```
2. Run ansible playbook to create EC2 instance (you can add -vvv at the end for verbose)
   ```sh
   ansible-playbook -i inventory.ini playbooks/aws_ec2_provisioning.yml --tags create_ec2
   ```
3. Run ansible playbook to list all EC2 instance and retrieve your new EC2 IP (you can add -vvv at the end for verbose)
   ```sh
   ansible-playbook -i inventory.ini playbooks/aws_ec2_provisioning.yml --tags facts
   ```
  * Result will be a big dictionary (last result is the newly created instance ) search for :
  ```sh
    Public IP address: my.ec2.ip
  ```
4. Replace the line "#IP OF EC2" in the inventory.ini file for the new ip
5. Run ansible playbook to configure and deploy app (you can add -vvv at the end for verbose)
   ```sh
   ansible-playbook -i inventory.ini playbooks/aws_app_deployment.yml 
   ```
6. Check your new app (change ip dots "." for dashes "-")
  ```sh
   http://ec2-user@ec2-myIpAdresse.eu-west-1.compute.amazonaws.com
   ```

<p align="right">(<a href="#top">back to top</a>)</p>
